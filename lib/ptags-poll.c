#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <poll.h>

#include "ptags.h"

void
callback (int handle, pid_t pid, void *data)
{
  printf ("{{{h%d-p%d}}}", handle, (int) pid);
}

int
main (int ac, char **av)
{
  int i, n, k;
  int *hndl;
  const char *const *t;
  struct pollfd p;

  for (i = 1; i < ac && strcmp (av[i], "--"); i++);

  n = i - 1;
  hndl = alloca (n * sizeof *hndl);
  for (i = 0; i < n; i++)
    {
      hndl[i] = ptags_watch_get ((pid_t) atoi (av[i + 1]));
      ptags_watch_notify (hndl[i], (struct ptags_cbdef)
			  {
			  callback, NULL});
    }

  t = (const char *const *) &av[n + 1];
  if (*t)
    t++;
  k = ac - n - 2;
  printf ("seeking tag(s):\n");
  for (i = 0; t[i]; i++)
    printf (" - %d/%d: %s\n", i, k, t[i]);
  p.fd = ptags_watchfd ();
  p.events = POLLIN;
  for (;;)
    {
      printf ("\nsome  all  pid\n");
      for (i = 0; i < n; i++)
	{
	  printf (" %3s  %3s  %s\n",
		  ptags_watch_has_any (hndl[i], k, t) == 1 ? "yes" : "no ",
		  ptags_watch_has_all (hndl[i], k, t) == 1 ? "yes" : "no ",
		  av[i + 1]);
	}
      poll (&p, 1, -1);
      ptags_watch_sync ();
    }
  return 0;
}
